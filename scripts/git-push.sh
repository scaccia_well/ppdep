#!/bin/bash

apk add --no-cache openssh
eval $(ssh-agent -s)
echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
git remote set-url --push origin git@gitlab.com:$CI_PROJECT_PATH
git remote show origin
git checkout $CI_COMMIT_REF_NAME
git config --global user.email 'alfredo.scaccialepre@well.ch'
git config --global user.name 'Alfredo Scaccialepre'
git add --all && git commit -m "Edit manifest file for env $___ENVIRONMENT_NAME"
git push
#TODO: create different SSH public/private key pair