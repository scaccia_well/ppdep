#!/bin/bash
GITLAB_PROJECTS_TO_DEPLOY=( 'pp1' 'pp2' )

dockerSetup(){
  docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
}


getImageBranchOrMain(){
  local project_name=$1
  local branch=$2
  local image_on_main="registry.gitlab.com/scaccia_well/$project_name/main:latest"
  local image_name=$image_on_main

  if [ "$branch" == "main" ]; then
    echo "Branch is main. Will deploy image $image_name" >&2
  else
    set -e;
    EXIT_CODE=0;
    local image_on_branch="registry.gitlab.com/scaccia_well/$project_name/$branch:latest"
    docker manifest inspect "$image_on_branch" 1>/dev/null 2> /dev/null || EXIT_CODE=$?;
    if [ $EXIT_CODE -eq 0 ]; then
      echo "Image $image_on_branch exists. Will use it." >&2
      image_name=$image_on_branch
    else
      echo "Image $image_on_branch does not exist. Will default to registry.gitlab.com/scaccia_well/$project_name/main:latest" >&2;
    fi
  fi
  echo "$image_name"
}



dockerSetup
mkdir -p "./manifests/$___ENVIRONMENT_NAME"

for project_name in "${GITLAB_PROJECTS_TO_DEPLOY[@]}"
do
  image_full_name=$(getImageBranchOrMain "$project_name" "$___MICROSERVICE_BRANCH")
  helm template \
  --set IMAGE_FULL_NAME="$image_full_name" \
  --set ENVIRONMENT_NAME="$___ENVIRONMENT_NAME"  "$project_name" ./well-charts --namespace "$___ENVIRONMENT_NAME" \
  --set MICROSERVICE_NAME="$project_name" \
   > "./manifests/$___ENVIRONMENT_NAME/$project_name.yaml"
done